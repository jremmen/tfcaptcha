<?php 

class tfCaptcha {

	private $code;
	private $font;
	private $characters;

	public function __construct($characters, $font = 'ELRIOTT2.TTF') {
		$this->font = sprintf('./%s', $font);
		$this->$characters = $characters;
		$this->code = $this->setCode($characters);
	}

	public function setCode($n) {
		$pool = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
		function iter($i, $n, $acc, $pool) {
			if($i > $n) return $acc;
			else return iter($i += 1, $n, $acc .= $pool[array_rand($pool)], $pool);
		}
		return iter(0, $n, '', $pool);
	}

	private function randomBackground($prefix, $range) {
		return sprintf('%s%s.png', $prefix, rand(0, $range));
	}

	private function createImage() {
		$img = imagecreatefrompng($this->randomBackground('bg', 3));
		$tc = imagecolorallocate($img, 42, 42, 42);
		$s = imagettfbbox(52, 5, $this->font, $this->code);
		$x = $s[0] + (imagesx($img) / 2) - ($s[4] / 2);
		$y = $s[1] + (imagesy($img) / 2) - ($s[5] / 2) - 10;
		imagettftext($img, 52, 5, $x, $y, $tc, $this->font, $this->code);
		ob_start();
		imagepng($img);
		return ob_get_clean();
	}

	public function getEncodedCaptchaImage() {
		return base64_encode($this->createImage());
	}

	public function getCaptchaCode() {
		return $this->code;
	}

}

$captcha = new tfCaptcha(8, 'Alice_in_Wonderland_3.ttf');
$img = $captcha->getEncodedCaptchaImage();
printf('<img src="data:image/png;base64,%s"/>', $img);
printf('<p>%s</p>', $captcha->getCaptchaCode());

?>

<style>
img { border: 2px solid #333; border-radius: .25em; }
</style>
